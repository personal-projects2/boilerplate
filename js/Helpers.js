/* Preload Images */
export const preloadImages = (url, newArr, slideShow, callback) => {
  let img;
  let fimg = document.querySelector('.slideshow img')
  let rangeslider = document.querySelector('.image-viewer input[type="range"]');

  for (let i = 0; i < url.length; i++) {
    img = new Image();

    img.onload = () => {
      --url.length;
      if(url.length <= 0) {
        callback(fimg);
        fimg.src = newArr[0]
        fimg.classList.remove('notLoaded')
        fimg.classList.add('loaded')
        rangeslider.classList.add('loaded')
      }
    }

    img.src = url[i];
    img.id = i
    newArr.push(img.src);
  }
}

export const showFrame = (newFrame, loadedImgs, type) => {
    setTimeout(() => {
      document.querySelector('.slideshow img').src = loadedImgs[newFrame]
    }, 100);
}


/* Modulus */
export let modulus = (x, y) => {
  return ((x % y) + y) % y
}