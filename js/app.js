import { preloadImages, showFrame, modulus } from './Helpers.js'

/**
 *
 * Variables
 *
 */

const slideShow = document.querySelector('.slideshow'),
      placeHolder = document.querySelector('img.placeholder'),
      rangeSlider = document.querySelector('input[type="range"]'),
      playButton = document.querySelector('.play');

const options = {
        fileName: slideShow.dataset.name,
        fileExtension: slideShow.dataset.extension,
        fileSeperator: slideShow.dataset.seperator,
        filePath: placeHolder.src.substring(0, placeHolder.src.lastIndexOf("/") + 1),
        frames: slideShow.dataset.frames,
      }

let gatherImgs = [],
    loadedImgs = [],
    firstImage = null,
    currentFrame = 0,
    mouseDownPos = 0,
    playing = false,
    autoPlayIteration = 0,
    intervalTimer = 100,
    interval;

/**
 *
 * Set placeholder opacity
 * Gather all images and push it into gatherImgs arr
 * call ImagesReady when preloadImages is done
 *
 */
placeHolder.style.opacity = '.4';

for (let i = 0; i < options.frames; i++) {
  gatherImgs.push(options.filePath+options.fileName+options.fileSeperator+(i+1)+'.'+options.fileExtension);
}

const imagesReady = (fimg) => {
  firstImage = fimg;
  placeHolder.style.opacity = '0';
  rangeSlider.max = options.frames; // Set max value on range slider equal to loadedImgs arr length
  options.frames < 25 ? intervalTimer = 100 : intervalTimer = 50;
}

preloadImages(gatherImgs, loadedImgs, slideShow, imagesReady);



/**
 *
 * Slideshow
 * Calculate the newFrame based on the draggedPosition
 * and switch frames by using opacity 0-1
 *
 */

let calculateFrame = (e) =>  {
  e.preventDefault();

  let diff = e.pageX - mouseDownPos;
  let draggedPosition = diff / 15;
  let newFrame = modulus(currentFrame + Math.floor(draggedPosition), options.frames - 1);
  autoPlayIteration = newFrame // autplayIteration = current frame


  setTimeout(() => {
    rangeSlider.max = options.frames - 1
    rangeSlider.value = newFrame

    if((Number(rangeSlider.max) - Number(rangeSlider.value)) == 1) {
      rangeSlider.value += 1
      newFrame += 1
    }
  }, 100)


  showFrame(newFrame, loadedImgs);
}

/**
 *
 * Autoplay
 * Start iterating over loadedImgs array and use setInterval
 * at Eventlisteners[e.key] to start the interval.
 *
 */

let showAutoPlayFrames = () => {
  document.querySelector('.slideshow img').src = loadedImgs[autoPlayIteration]

  rangeSlider.value = autoPlayIteration;
  if((Number(rangeSlider.max) - Number(rangeSlider.value)) == 1) {
    rangeSlider.value += 1
  }

  autoPlayIteration++;

  if(autoPlayIteration >= loadedImgs.length){ // hitting end of arr resets the iteration
    autoPlayIteration = 0;
  }
}

/**
 *
 * Eventlisteners
 * Mousedown, mousemove, mouseup, keypress[autoplay]
 *
 */
 playButton.addEventListener('click', (e) => {
  if(!playing) {
    playButton.classList.add('paused')
    interval = setInterval(showAutoPlayFrames, intervalTimer);
    playing = true;
  } else if(playing) {
    playButton.classList.remove('paused')
    clearInterval(interval)
    playing = false;
  }
 })

slideShow.addEventListener('mousedown', (e) => {
  e.preventDefault();
  mouseDownPos = e.offsetX;
  firstImage.classList.remove('loaded')

  slideShow.addEventListener('mousemove', calculateFrame);
});

window.addEventListener('mouseup', () => {
  slideShow.removeEventListener('mousemove', calculateFrame);
})

rangeSlider.addEventListener('input', (e) => {
  firstImage.classList.remove('loaded')
  autoPlayIteration = rangeSlider.value; // autplayIteration = current frame

  if(Number(rangeSlider.value) === loadedImgs.length) {
    rangeSlider.max -= 1
  }

  showFrame(e.target.value, loadedImgs); // Range slider value
})

