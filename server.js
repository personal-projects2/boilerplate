const express = require('express');
var path = require("path");
const app = express();

app.use(express.static('.'))

app.listen(8080, () => console.log('Listening on port 8080!'));